package com.company;

public enum Nipes {

    PAUS("PAUS"),
    AS("AS"),
    COPAS("COPAS"),
    OURO("OURO");

    private String nipes;

    Nipes(String nipes) {
        this.nipes = nipes;
    }

    public String getNipes() {
        return nipes;
    }

    public void setNipes(String nipes) {
        this.nipes = nipes;
    }
}
