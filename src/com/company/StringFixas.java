package com.company;

public interface StringFixas {

    String BEM_VINDO = "BEM VINDO AO JOGO DE 21";
    String QUESTIONA_SE_CONTINUA_JOGO = "Deseja jogar novante? (1 - SIM / 2 - NÃO)";
    String VALOR_NAO_IDENTIFICADO = "VALOR NÃO IDENTIFICADO - JOGO ENCERRADO";
    String QUESTIONA_QUANTIDADE_DE_FICHAS = "Quantas fichas deseja jogar?";
    String INICIANDO_JOGO = "Iniciando Jogo...";
    String SORTEANDO_VALORES = "Sorteando uma carta..";
    String VALORES_SORTEADOS_COM_SUCESSO = "Valores sorteados com sucesso...";
    String RESULTADO = "O seu resultado até o momento é: ";
    String O_QUE_FAZER = "O que deseja fazer?";
    String VALOR_SORTEADO = "Valor Sorteado: ";
    String PONTOS = ". Pontos: ";
    String SUA_PONTUACAO = "Sua Pontuação foi: ";
    String MENSAGEM_NOVO_SIMBOLO = "Vamos inserir um novo simbolo no sorteio!";
    String ENTRE_COM_O_SIMBOLO = "Entre com o nome do Simbolo";
    String VOCE_PERDEU = "Você Perdeu!";
}