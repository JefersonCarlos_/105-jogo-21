package com.company;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Baralho {

    private static int ValorAteAgora;
    private static int melhorResultado;
    private static HashMap<Nipes, NumerosBaralho> baralho_Completo = new HashMap<>();

    public static void montaBaralho() {
        ValorAteAgora = 0;
        for (int i = 0; i < Nipes.values().length; i++) {
            for (int j = 0; j < NumerosBaralho.values().length; j++) {
                baralho_Completo.put(Nipes.values()[i], NumerosBaralho.values()[j]);
            }
        }
    }

    public static void ObtemCarta() {
        boolean isSorteiaDeNovo = true;
        Nipes nipe;
        NumerosBaralho numero;
        do {
            Random random = new Random();
            nipe = Nipes.values()[random.nextInt(Nipes.values().length)];
            numero = NumerosBaralho.values()[random.nextInt(NumerosBaralho.values().length)];

            for (Map.Entry entry : baralho_Completo.entrySet()) {
                if (nipe.equals(entry.getKey()) && numero.equals(entry.getValue())) {
                    isSorteiaDeNovo = false;
                    baralho_Completo.remove(nipe, numero);
                }
            }
        } while (!isSorteiaDeNovo);

        SomaValorSorteio(numero.getValor_baralho());
        IO.exibeMensagem(StringFixas.VALOR_SORTEADO + numero.getValor_baralho() + " " + nipe.getNipes());
    }

    public static void SomaValorSorteio(int numero) {
        ValorAteAgora += ValorAteAgora + numero;
    }

    public static int getValorAteAgora() {
        return ValorAteAgora;
    }

    public static void setValorAteAgora(int valorAteAgora_) {
        ValorAteAgora = valorAteAgora_;
    }

    public static int getMelhorResultado() {
        return melhorResultado;
    }

    public static void setMelhorResultado(int melhorResultado) {
        Baralho.melhorResultado = melhorResultado;
    }
}
