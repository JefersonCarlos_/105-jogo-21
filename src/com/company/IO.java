package com.company;

import java.awt.datatransfer.StringSelection;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class IO {

    public static void exibeMensagem(String mensagem) {
        System.out.println(mensagem);
    }

    public void montaMenu() {
        exibeMensagem(StringFixas.O_QUE_FAZER);
        for (int i = 0; i < OpcoesMenu.values().length; i++) {
            exibeMensagem(String.valueOf(OpcoesMenu.values()[i]));
        }

        identificarMenuDesejado();
    }

    public void jogar() {
        Baralho.montaBaralho();
        do {
            exibeMensagem(StringFixas.INICIANDO_JOGO);
            exibeMensagem(StringFixas.SORTEANDO_VALORES);
            Baralho.ObtemCarta();
            exibeMensagem(StringFixas.RESULTADO + Baralho.getValorAteAgora());
            if (isVerificaSePerdeu()) {
                exibeMensagem(StringFixas.VOCE_PERDEU);
                montaMenu();
            }
        } while (isContinuaJogando());

        Baralho.setMelhorResultado(Baralho.getValorAteAgora());
        montaMenu();
    }

    public boolean isVerificaSePerdeu() {
        return (Baralho.getValorAteAgora() > 21);
    }

    public boolean isContinuaJogando() {
        exibeMensagem(StringFixas.QUESTIONA_SE_CONTINUA_JOGO);
        Scanner scanner = new Scanner(System.in);
        String valorLido = scanner.next();
        if (valorLido.equals("1")) {
            return true;
        } else if (valorLido.equals("2")) {
            return false;
        } else return false;
    }

    public void identificarMenuDesejado() {
        String opcaoDesejada = lerDadosUsuario();
        if (opcaoDesejada.equals("1")) {
            jogar();
        } else if (opcaoDesejada.equals("3")) {
            sair();
        }
    }

    public void sair() {
        System.exit(0);
    }

    public static String lerDadosUsuario() {
        try {
            Scanner scanner = new Scanner(System.in);
            return scanner.next();
        } catch (Exception e) {
            IO.exibeMensagem(StringFixas.VALOR_NAO_IDENTIFICADO);
        }
        return "";
    }


}
