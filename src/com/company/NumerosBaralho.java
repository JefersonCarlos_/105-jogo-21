package com.company;

public enum NumerosBaralho {

    UM(1),
    DOIS(2),
    TRES(3),
    QUATRO(4),
    CINCO(5),
    SEIS(6),
    SETE(7),
    OITO(8),
    NOVE(9),
    DEZ(10);


    private int valor_baralho;

    NumerosBaralho(int valor_baralho) {
        this.valor_baralho = valor_baralho;
    }

    public int getValor_baralho() {
        return valor_baralho;
    }

    public void setValor_baralho(int valor_baralho) {
        this.valor_baralho = valor_baralho;
    }
}
